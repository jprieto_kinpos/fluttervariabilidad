import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/resource/display_strings_app1.dart';
import 'app_config.dart';
import 'main_common.dart';

void main() {

  var configuredApp = AppConfig(
    appDisplayName: "Aplicación 1",
    appInternalId: 1,
    stringResource: StringResourceApp1(),
    child: MyApp(),
  );

  mainCommon();

  runApp(configuredApp);
}
