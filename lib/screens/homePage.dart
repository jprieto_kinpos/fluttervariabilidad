import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app/resource/display_strings.dart';
import 'package:intl/intl.dart';

import '../app_config.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();

}

class _HomePageState extends State<HomePage> {
  static const platform = const MethodChannel('samples.flutter.io/app_name');

  @override
  Widget build(BuildContext context) {
    var config = AppConfig.of(context);//aqui trae las caracteristicaas del app que va corriendo

    _getAppName();

    return Scaffold(
      appBar: AppBar(
        title: Text(config.appDisplayName),
      ),
      body: _buildBody(("${config.appDisplayName}  $_appName"), config.stringResource, config.appInternalId),
    );
  }

  Widget _buildBody(String appName, StringResource stringResource, int appInternalId) {
    return Container(
        margin: EdgeInsets.symmetric(vertical: 16.0, horizontal: 16.0),
        child: Column(
          children: <Widget>[
            Text(appName),
            Text(stringResource.DATE/*DATE*/ + getDateForDisplay()),
            Text(stringResource.APP_DESCRIPTION),
            Image.asset('assets/dancing.png', width: 50.0, height: 50.0,),
            Image.asset('assets/' + appInternalId.toString() + '/icon.png', width: 50.0, height: 50.0),
          ],
        )
    );
  }

  String getDateForDisplay() {
    DateTime now = DateTime.now();
    var formatter = DateFormat('EEEE dd MMMM yyyy');
    return formatter.format(now);
  }

  String _appName = 'Unknown name.';

  Future<void> _getAppName() async {
    String appName;
    try {
      final String result = await platform.invokeMethod('getAppName');
      appName = result;
    } on PlatformException catch (e) {
      appName = "Failed to get app name: '${e.message}'.";
    }

    setState(() {
      _appName = appName;
    });
  }

}