import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
  ) -> Bool {
    
    let controller: FlutterViewController =  self.window.rootViewController as! FlutterViewController;
    let channel = FlutterMethodChannel(name: "samples.flutter.io/app_name", binaryMessenger: controller as! FlutterBinaryMessenger)
    
    channel.setMethodCallHandler { (call, result) in
        // 3. switch on method and (optionally) arguments
        switch call.method {
        case "getAppName":
            // do some work, then:
            // 4. call completion handler
            result(self.getAppName())
        default:
            result(FlutterMethodNotImplemented);
            break
        }
    }

    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    func getAppName() -> String{
        return "plataforma IOS"
    }
}
